#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Define distribution functions

This file is part of the project `psd_analysis`.
Copyright (c) 2019-2023, GPL v3, KIT/IMK-AAF (Karlsruhe Institute of Technology,
Institute of Meteorology and Climate Research, Atmospheric Aerosol Research)
For full license details see the file LICENSE.md or go to
https://www.gnu.org/licenses/gpl-3.0.html.
"""
import numpy as np


def lognormal(x, mu, sig, tot):
    """Create a lognormal distribution with median mu and geometric standard
    deviation sig, both on the natural scale.
    
    Reference:
        Limpert, E.; Stahel, W. A.; Abbt, M. (2001). "Log-normal
        distributions across the sciences: Keys and clues".
        BioScience. 51 (5): 341–352."""
    return ((tot / (np.log10(sig) * np.sqrt(2 * np.pi)))
            * np.exp(-((np.log10(x / mu))**2) / (2 * np.log10(sig)**2)))


def bilognormal(x, mu1, sig1, N1, mu2, sig2, N2):
    """Create a bilognormal distribution.
    It is simply a superposition of two lognormal functions."""
    return ((N1 / (np.log10(sig1) * np.sqrt(2 * np.pi)))
            * np.exp(-((np.log10(x / mu1))**2) / (2 * np.log10(sig1)**2))
            + (N2 / (np.log10(sig2) * np.sqrt(2 * np.pi)))
            * np.exp(-((np.log10(x / mu2))**2) / (2 * np.log10(sig2)**2)))
